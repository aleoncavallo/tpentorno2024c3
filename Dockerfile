# Imagen base de la que partimos
FROM ubuntu:24.04

# Instalación de paquetes de Ubuntu
RUN apt-get update
RUN apt-get install --no-install-recommends -y python3-pip libgl1 libglib2.0-0

# Para poder manejar instalaciones con pip, porque en apt no encontramos ultralytics
RUN rm -f /usr/lib/python3*/EXTERNALLY-MANAGED

# Instalación de módulos de python para YOLO, específico para correr sin GPU
RUN pip install --no-cache torch torchvision --index-url https://download.pytorch.org/whl/cpu

# Yolo en sí mismo
RUN pip install --no-cache ultralytics

# Instalación del modelo YOLO
COPY ./models/yolov8l.pt /usr/src/app/

# Configuración de YOLO
# Fija el modelo yolov8l
RUN sed -i "s,model:,model: /usr/src/app/yolov8l.pt," /usr/local/lib/python3*/dist-packages/ultralytics/cfg/default.yaml
# Cambia el directorio donde se guardan las imágenes, por ahora no vamos a usar
# las imágenes generadas
RUN echo save_dir: /usr/src/app >> /usr/local/lib/python3.12/dist-packages/ultralytics/cfg/default.yaml

# A continuación instalar los programas faltantes para los distintos ejercicios
############################# COMPLETAR DESDE AQUÍ #############################

############################# COMPLETAR HASTA AQUÍ #############################

# Configuración de la aplicación
ENV TERM=xterm
ENV COLORTERM=24bit
COPY ["src/", "/app/"]
WORKDIR /app
ENTRYPOINT ["/app/main.sh"]
