# Trabajo Práctico Final

## Introducción

### Descripción

El objetivo del trabajo práctico es usar `git` y `docker` correctamente mientras
se completa un programa para descargar y etiquetar un lote de imágenes.

Este programa consta de cuatro partes principales:

1. Obtención de imágenes.
2. Generación de etiquetas.
3. Mostrar imágenes de determinada etiqueta.
4. Generación de un archivo comprimido con las imágenes y etiquetas.

Todo el trabajo debe ser realizado bajo control de versiones, con participación
de **todos** los integrantes y debe ejecutarse dentro del contenedor. 
Luego **todos** los integrates deberán ser capaces de explicar todos los scripts
involucrados aún cuando los hayan hecho sus compañeros.

### Pautas generales

* El trabajo práctico deberá ser realizado en grupos de dos (o tres como excepción).
* Deberán crear un fork de este proyecto desde una de sus cuentas de gitlab y 
compartirlo con el otro integrante del grupo con permiso owner y con permiso reporter 
a fceiatuia. 
* Todos los integrantes deben conocer todos los aspectos del trabajo entregado.
* Se admite una única entrega final, es por ello que solicitamos revisen muy
bien las funcionalidades previamente a la entrega formal.

## Enunciado

Deberán modificar un contenedor que al ejecutarse presenta un menú de opciones.
Para ello será necesario editar el `Dockerfile` y construir la imagen.

Las imágenes a analizar deben ser descargadas desde internet y deben almacenarse
dentro del contenedor para su posterior análisis. Luego del análisis, deberá
generarse un archivo disponible fuera del contenedor.

Tanto los scripts, como el `Dockerfile`, deben estar en el repositorio gitlab
que creen para realizar el desarrollo en equipo.

Deberán trabajar en el repositorio manteniendo la prolijidad y las buenas
prácticas de `git`: por cada script se deberá crear una rama para luego ir
integrando a la rama principal a medida que los scripts estén listos. Todos los
integrantes deben realizar commits en el repositorio.

En ese mismo repositorio debe estar la documentación suficiente para comprender
cómo desplegar el contenedor y ejecutar la aplicación. Esta documentación debe
estar en un archivo `README.md` el cual estará presente cuando se accede al
repositorio en gitlab.

### Scripts

Se deberán programar los siguientes scripts:

* `internet.sh`: Chequea que haya conexión a internet.

* `descargar.sh`: Descarga una imagen de internet y la nombra convenientemente.

* `etiquetar.sh`: Genera un archivo con las etiquetas de una imagen.

* `mostrar.sh`: Muestra las imágenes que contienen objetos de una etiqueta dada.

* `comprimir.sh`: Comprime todas las imágenes.

* `extra.sh`: Funcionalidad adicional a elección de cada grupo.

### Estructura de directorios

El programa está estructurado en varias carpetas para que sea mas sencillo
trabajar. Dentro de cada una de ellas se encuentra la documentación específica
de dicha carpeta.

Los únicos archivos que debe modificar el estudiante son el archivo
`Dockerfile`, el archivo `README.md` y los scripts de la carpeta `src/scripts`.
No deben modificar los scripts de `src/menu` pero es necesario que los lean y
los puedan explicar.

La carpeta `./imagenes` debe poder accederse dentro del contenedor en la ruta
`/imagenes`.

### Herramientas útiles

#### Descarga de imágenes

Explore los siguientes enlaces para descargar imágenes al azar:
  * https://random-image-pepebigotes.vercel.app/api/random-image
  * https://image.pollinations.ai/prompt/a%20photo%20of%20a%20person?seed=1

Realice una búsqueda web para encontrar recursos similares y elija su favorito.

#### Yolo

Yolo es un modelo de I.A. que puede utilizarse para clasificar imágenes. **Dentro
del contenedor** puede utilizarse con el siguiente comando:
```bash
yolo predict source=/ruta/archivo.jpg
```

#### jp2a
`jp2a` Es una herramienta para convertir imágenes en caracteres ASCII. Será útil
para ver las imágenes desde dentro de la consola. Deberá investigar la instalación 
y uso de la herramienta.

### Respecto a etiquetar con Yolo:

Yolo etiqueta los objetos que se encuentran en una imagen y nos devuelve líneas
con la siguiente apariencia:
```
Downloading https://github.com/ultralytics/assets/releases/download/v8.1.0/yolov8n.pt to 'yolov8n.pt'...
100%|████████████████████████████████████████████| 6.23M/6.23M [00:04<00:00, 1.37MB/s]
Ultralytics YOLOv8.1.47 🚀 Python-3.8.10 torch-2.0.1+cu117 CPU (Intel Core(TM) i5-5300U 2.30GHz)
YOLOv8n summary (fused): 168 layers, 3151904 parameters, 0 gradients, 8.7 GFLOPs

image 1/1 /home/andrea/entorno/tp/ff417926378038bb08cd3dc92bcae2fa5cfd30919f6b5e4af242b9436ae4b29a.jpg: 448x640 2 persons, 1 potted plant, 1 laptop, 2 books, 1008.0ms
Speed: 43.4ms preprocess, 1008.0ms inference, 2292.1ms postprocess per image at shape (1, 3, 448, 640)
```

Consideraremos etiquetas en este ejemplo a: `persons`, `potted plant`, `laptop`
y `books`.
Definimos por etiqueta principal a la primera que aparece, en este caso sería:
`persons`.

En algunos casos, Yolo no nos devuelve ninguna etiqueta, un ejemplo de salida de
esto es el siguiente:
```
Ultralytics YOLOv8.1.47 🚀 Python-3.8.10 torch-2.0.1+cu117 CPU (Intel Core(TM) i5-5300U 2.30GHz)
YOLOv8n summary (fused): 168 layers, 3151904 parameters, 0 gradients, 8.7 GFLOPs

image 1/1 /home/andrea/entorno/tp/1bdcbfeea2c5345f0fe9c781da9684cf9fe8ec19091cb0f21e80e5a9aaeb4173.jpg: 640x480 (no detections), 437.6ms
Speed: 35.0ms preprocess, 437.6ms inference, 264.7ms postprocess per image at shape (1, 3, 640, 480)
```

En este caso consideraremos que tiene una única etiqueta (es decir la etiqueta
principal) que llamaremos: `no_detections`.

### Respecto a la separación de etiquetas:

Se pide que los archivos terminados en `.tag` tengan una estructura del estilo
`etiqueta1`, `etiqueta2`, `...`.

Para quitar el último campo puede ser útil usar el comando `rev`.

