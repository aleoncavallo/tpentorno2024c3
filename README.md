# Trabajo Práctico Recursado de Entorno de Programación (2024)

Este repositorio es un trabajo práctico para la materia "Entorno de
Programación" de la carrera T.U.I.A. en F.C.E.I.A. (U.N.R.).

Aquí puede leerse el [enunciado](docs/enunciado.md) del trabajo.


## Integrantes del grupo

* Integrante 1.
* Integrante 2.


## Instrucciones

### Ejecución fuera del contenedor

Puede correr el programa fuera del contenedor:
```bash
./src/main.sh
```

Para ejecutar la parte que depende de Yolo la cátedra ha preparado un Dokerfile
con instrucciones que lo instalan gastando muchos menos recursos de lo habitual.

A continuación están las instrucciones para instalar Docker, habilitar el servicio 
asociado, agregar el usuario actual al grupo Docker (para ejecutarlo sin usar sudo 
o usuario root), crear la imagen a partir del Dokerfile y correr el código dentro 
del contenedor. 

### Ejecución dentro del contenedor

### Dependencias

Es necesario tener instalados `docker` y `docker buildx` para poder ejecutar
este programa. En distribuciones basadas en Ubuntu esto puede conseguirse así:
```bash
sudo apt update
sudo apt install docker.io docker-buildx
```

Luego será necesario habilitar el servicio de contenedores de docker:
```bash
sudo systemctl enable docker
sudo systemctl start docker
```

También será necesario agregar el usuario actual al grupo `docker`:
```bash
sudo usermod -aG docker $USER
```

Para que este cambio surja efecto, es necesario salir de la sesión 
y luego volver a entrar


### Ejecución dentro del contenedor

Para poder correr el programa en el contenedor primero debe construir la imagen:
```bash
docker buildx build -t entorno .
```

Luego puede ejecutar el contenedor con el siguiente comando:
```bash
docker run -it entorno
```


Una vez andando el contenedor puede conectarse al mismo con exec:
```bash
docker exec -it nombre_contenedor_activo bash
